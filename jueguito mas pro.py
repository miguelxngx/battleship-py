 #!/usr/bin/python
#coding=utf-8
import copy, random

def imprime_archivo(nombre_archivo):
        archivo = open(nombre_archivo, "r")
        c = archivo.read()
        print c
        archivo.close()
    
def menu():
        imprime_archivo("menu_python.txt")

def instrucciones():
        imprime_archivo("instrucciones_python.txt")

def imprimir_tablero(s, tablero):
	tablero_imp = "Tablero PC"
	if s == "u":
		tablero_imp = "Tablero user"
	
	print tablero_imp

	print " ",
	for i in range(10):
		print "  " + str(i+1) + "  ",
	print "\n"

	for i in range(10):
	
		if i != 9: 
			print str(i+1) + "  ",
		else:
			print str(i+1) + " ",

		
		for j in range(10):
			if tablero[i][j] == -1:
				print ' ',	
			elif s == "u":
				print tablero[i][j],
			elif s == "c":
				if tablero[i][j] == "*" or tablero[i][j] == "X":
					print tablero[i][j],
				else:
					print " ",
			
			if j != 9:
				print " | ",
		print
		
		if i != 9:
			print "   ----------------------------------------------------------"
		else: 
			print 

def obtenerBarcosUser(tablero,barcos):

	for i in barcos.keys():

		#obtiene las coordenadas y checa si son correctas
		res = False
		while(not res):

			imprimir_tablero("u",tablero)
			print "Colocando un " + i
			col,ren = obtener_coordenadas()
			ori = v_or_h()
			res = checar_datos(tablero,barcos[i],col,ren,ori)
			if not res:
				print "No se puede colocar un barco aquí."
				raw_input("Presiona ENTER para continuar")

		#coloca el barquito
		tablero = llenar_espacios(tablero,barcos[i],i[0],ori,col,ren)
		imprimir_tablero("u",tablero)
		
	raw_input("Presiona ENTER para empezar el juego")
	return tablero


def inicializa_objetivosCompu(tablero,barcos):

	for i in barcos.keys():
	
		#coordenadas random y checa si si se puede
		res = False
		while(not res):

			col = random.randint(1,10)-1
			ren = random.randint(1,10)-1
			pos = random.randint(0,1)
			if pos == 0: 
				ori = "v"
			else:
				ori = "h"
			res = checar_datos(tablero,barcos[i],col,ren,ori)

		#coloca el barquito
		tablero = llenar_espacios(tablero,barcos[i],i[0],ori,col,ren)
	
	return tablero


def llenar_espacios(tablero,lugares,s,ori,col,ren):

	#coloca al barquito dependiendo de la psición
	if ori == "v":
		for i in range(lugares):
			tablero[col+i][ren] = s
	elif ori == "h":
		for i in range(lugares):
			tablero[col][ren+i] = s

	return tablero
	
def checar_datos(tablero,lugares,col,ren,ori):

	#checa si el barquito se puede poner en las coordenadas dadas
	if ori == "v" and col+lugares > 10:
		return False
	elif ori == "h" and ren+lugares > 10:
		return False
	else:
		if ori == "v":
			for i in range(lugares):
				if tablero[col+i][ren] != -1:
					return False
		elif ori == "h":
			for i in range(lugares):
				if tablero[col][ren+i] != -1:
					return False
		
	return True

def v_or_h():

	#obtiene la orientación
	while(True):
		user_input = raw_input("vertical o horizontal (v,h)-> ")
		if user_input == "v" or user_input == "h":
			return user_input
		else:
			print "Respuesta invalida, por favor ingrese v o h "

def obtener_coordenadas():
	
	while (True):
		user_input = raw_input("Ingrese las coordenadas (renglon,columna)-> ")
		try:
			#Checa que el usuario haya ingresado dos valores separados por una coma
			coordenada = user_input.split(",")
			if len(coordenada) != 2:
				raise Exception("Respuesta invalida");

			#checa que los dos valores sean numeros enteros
			coordenada[0] = int(coordenada[0])-1
			coordenada[1] = int(coordenada[1])-1

			#checa que los valores sean entre 1 y 10 en ambas coordenadas
			if coordenada[0] > 9 or coordenada[0] < 0 or coordenada[1] > 9 or coordenada[1] < 0:
				raise Exception("Respuesta invalida")

			#regresa las coordenadas si no hay error
			return coordenada
		
		except ValueError:
			print "Respuesta invalida"
		except Exception as e:
			print e

def disparar(tablero,col,ren):
	
	#dispara al tablero y dice si le dio, o fallo o ya había disparado ahí
	if tablero[col][ren] == -1:
		return "fallaste"
	elif tablero[col][ren] == '*' or tablero[col][ren] == 'X':
		return "intenta de nuevo"
	else:
		return "hit"

def disparar_user(tablero):
	
	#obtiene las coordenadas del user
	#si el disparo le da a algo checa si ya se hundió
	while(True):
		col,ren = obtener_coordenadas()
		res = disparar(tablero,col,ren)
		if res == "hit":
			print "Le diste en " + str(col+1) + "," + str(ren+1)
			hundir(tablero,col,ren)
			tablero[col][ren] = 'X'
			if checar_resultado(tablero):
				return "VICTORIA"
		elif res == "fallaste":
			tablero[col][ren] = "*"
		elif res == "intenta de nuevo":
			print "Ya se había disparado aqui"	

		if res != "intenta de nuevo":
			return tablero

def turnoPC(tablero):
	
	#genera coordenadas random
	while(True):
		col = random.randint(1,10)-1
		ren = random.randint(1,10)-1
		res = disparar(tablero,col,ren)
		if res == "hit":
			print "Le dio a un barco en " + str(col+1) + "," + str(ren+1)
			hundir(tablero,col,ren)
			tablero[col][ren] = 'X'
			if checar_resultado(tablero):
				return "VICTORIA"
		elif res == "fallaste":
			tablero[col][ren] = "*"

		if res != "intenta de nuevo":
			
			return tablero
	
def hundir(tablero,col,ren):

	#define cual barco fue al que se le dió
	if tablero[col][ren] == "C":
		barco = "Carrier"
	elif tablero[col][ren] == "B":
		barco = "Buque"
	elif tablero[col][ren] == "S":
		barco = "Submarino" 
	elif tablero[col][ren] == "D":
		barco = "Destructor"
	elif tablero[col][ren] == "P": 
		barco = "Patrulla"
	
	#marca la coordenada como  que ya se le dio y checa si ya se hundió
	tablero[-1][barco] -= 1
	if tablero[-1][barco] == 0:
		print barco + " Hundido"
		

def checar_resultado(tablero):
	
	for i in range(10):
		for j in range(10):
			if tablero[i][j] != -1 and tablero[i][j] != '*' and tablero[i][j] != 'X':
				return False
	return True

def juego():

	#tipos de barcos
	barcos = {"Carrier":5,
		     "Buque":4,
 		     "Submarino":3,
		     "Destructor":3,
		     "Patrulla":2}

	#crea un tablero 10x10 en blanco
	tablero = []
	for i in range(10):
		tablero_oculto = []
		for j in range(10):
			tablero_oculto.append(-1)
		tablero.append(tablero_oculto)

	#copia el tablero predeterminado y crea el tablero del user y el de la pc
	tablero_user = copy.deepcopy(tablero)
	tablero_pc = copy.deepcopy(tablero)

	#agrega los barcos como ultimo elemento del array
	tablero_user.append(copy.deepcopy(barcos))
	tablero_pc.append(copy.deepcopy(barcos))

	#coloca los barcos
	tablero_user = obtenerBarcosUser(tablero_user,barcos)
	tablero_pc = inicializa_objetivosCompu(tablero_pc,barcos)

	#loop de juego
	while(1):

		#turno user
		imprimir_tablero("c",tablero_pc)
		tablero_pc = disparar_user(tablero_pc)

		#checar si el usuario ganó
		if tablero_pc == "VICTORIA":
			print "¡¡¡VICTORIA!!!"
			quit()
			
		#imprime el tablero de la pc
		imprimir_tablero("c",tablero_pc)
		raw_input("Presiona ENTER para continuar con el turno de la pc")

		#turno oc
		tablero_user = turnoPC(tablero_user)
		
		#checa si la compu ganó
		if tablero_user == "VICTORIA":
			print "DIME, ¿QUE SE SIENTE DESHONRAR TANTOS AÑOS EVOLUTIVOS?"
			quit()
			
		#muestra el tablero del user
		imprimir_tablero("u",tablero_user)
		raw_input("Presiona ENTER para continuar tu turno")

def main():
        opc = '0'
        while opc != '3':
                menu()
                opc = raw_input()

                if opc == '1':
                        juego()
                elif opc == '2':
                        instrucciones()
                        raw_input()
                elif opc == '3':
                        quit()
        
if __name__=="__main__":
	main()
